TagCloud Plugin for CakePHP 2.x
Version 1.1

Master: [https://gitlab.com/cakephp-2-x-plugins/tagcloud](https://gitlab.com/cakephp-2-x-plugins/tagcloud)

# General Information

The TagCloud plugin allows for two major features.  
- Adding Tag support to your existing models.
- Quickly generating tag clouds for your pages.

# Release Information

## Version 1.1
- Added support for baking the schema.

## Version 1.0
- Initial public release.

## Setup

Load the plugin in *app/Config/bootstrap.php*:
``` php
CakePlugin::load('TagCloud');
```
### TagCloudHelper

Add the Helper and set some defaults in your Controller:
``` php
public $helpers = array('TagCloud.TagCloud' => array $settings);
```

#### Possible Settings:
- **url_function**: A callback function for generating a link instead of just a plain tag. (Default: false)

- **min_size**: The minimum font size to use in pixels. (Default: 10)
- **min_size**: The maximum font size to use in pixels. (Default: 50)

- **red**: Base red in text color to use, 0-255 (Default 110)
- **green**: Base green in text color to use, 0-255 (Default 105)
- **blue**: Base blue in text color to use, 0-255 (Default 255)

- **div_size**: The size of the div surrounding the cloud. (Default: 100%)

- **show_frequencies**: Display the actual numbers for each tag. (Default: false)
- **shuffle**: Sort the tags in a random order. (Default: true)

### Tag & TagBehavior

In your model that needs to have Tag support:
``` php
class TestModel extends AppModel {
	public $actsAs = array('TagCloud.Tag');
	public $hasAndBelongsToMany = array(
			'TestTag' => array('className' => 'TagCloud.Tag'));
}
```

The key, in this case *TestTag*, must be named as **ParentModel**Tag.  This allows the Tag Model to automatically detect it.  This
also allows you to use Tag with several models at once. 

### Databases

To use TagCloud, you will need at least two databases.  One for holding the tags, and then one for each model to hold relationships.

You will also need to add a **tags** field to your existing model with a data type of TEXT to hold an easier to read version of the
tags and to facilitate processing. The name of this field can be overridden in the TagBehavior with the *tagLabel* setting, but it
is not fully tested or recommended.

Tag table:
``` mysql
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
)
```

Relationship table: (in this example, for the Test model) 
``` mysql
CREATE TABLE `tests_tags` (
  `id` varchar(36) NOT NULL,
  `test_id` varchar(36) NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
)
```
**Note:** the second field's data type will vary, based on your existing model.

### Schema Creation via Console

If you'd rather automate the creation of your tables for the plugin, a console shell has been provided to help you.

To create the tables, use the following command. **[Model List]** should be a comma seperated list of the Models where you want to add Tags.
```
cake tag_cloud.tag_schema create --models [Model List] 
```

For more information, run:
```
cake tag_cloud.tag_schema --help
```

## Usage

### Adding tags when saving.
If the data is saved to the *tags* field of the parent model, the TagBehavior will handle the rest.

### Showing clouds
To show a cloud of all tags on a page, first add the following code to your *Controller* where **Model** is the name of your *Model*:
```php
$cloudData = $this->Model->ModelTag->all(false);
$this->set(compact('cloudData'));
```

Then in your *View*, use the following code to display the cloud:
``` php
echo $this->TagCloud->show($cloudData);
```

Or, if you wish to add links to the tags:
``` php
echo $this->TagCloud->show($cloudData, array('url_function' => callback_url_function));
```

### Example of a simple Url Function
``` php
function callback_url_function($tag) {
	return '/view/by/tag/' . $tag;
}
```

## License
This plugin is released under the GPLv2.  You can find a copy of this licenese in [LICENSE.md](LICENSE.md).