<?php
/**
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @version 1.1
 *
 */
class TagCloudSchema extends CakeSchema {
	/**
	 *
	 * @var array $tags
	 */
	public $tags = array(
			'id' => array(
					'type' => 'integer',
					'null' => false,
					'default' => null,
					'length' => 10,
					'key' => 'primary',
					'collate' => 'utf8_general_ci',
					'charset' => 'utf8'),
			'tag' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 128),
			'indexes' => array(
					'PRIMARY' => array('column' => 'id', 'unique' => 1),
					'tag_index' => array('column' => 'tag', 'unique' => 1)),
			'tableParameters' => array(
					'charset' => 'utf8',
					'collate' => 'utf8_general_ci',
					'engine' => 'InnoDB'));
	/**
	 *
	 * @var array $model_tags
	 */
	public $model_tags = array(
			'id' => array(
					'type' => 'string',
					'null' => false,
					'default' => null,
					'length' => 36,
					'key' => 'primary',
					'collate' => 'utf8_general_ci',
					'charset' => 'utf8'),
			'tag_id' => array(
					'type' => 'integer',
					'null' => false,
					'default' => null,
					'key' => 'index',
					'length' => 10,
					'unsigned' => true),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'tag_index' => array('column' => 'tag_id')),
			'tableParameters' => array(
					'charset' => 'utf8',
					'collate' => 'utf8_general_ci',
					'engine' => 'InnoDB'));
}
