<?php
App::uses('AppShell', 'Console/Command');
App::uses('CakeSchema', 'Model');

/**
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @version 1.1
 */
class TagSchemaShell extends AppShell {
	/**
	 * Schema class being used.
	 *
	 * @var CakeSchema
	 */
	public $Schema;
	/**
	 * is this a dry run?
	 *
	 * @var bool
	 */
	protected $_dry = null;
	/**
	 * Override startup
	 *
	 * @return void
	 */
	public function startup() {
		$this->_welcome();
		$this->out('Tag Schema Shell');
		$this->hr();

		Configure::write('Cache.disable', 1);

		$name = $path = $connection = $plugin = null;
		$name = $this->params['name'] = 'TagCloud';
		$plugin = $this->params['plugin'] = 'TagCloud';

		$file = $this->params['file'] = 'schema.php';

		if (!empty($this->params['connection'])) {
			$connection = $this->params['connection'];
		}

		$name = Inflector::classify($name);
		$this->Schema = new CakeSchema(compact('name', 'path', 'file', 'connection', 'plugin'));
	}

	/**
	 * Run database create commands.
	 * Alias for run create.
	 *
	 * @return void
	 */
	public function create() {
		list ( $Schema, $table ) = $this->_loadSchema();
		$this->_create($Schema, $table);
	}

	/**
	 * Run database create commands.
	 * Alias for run create.
	 *
	 * @return void
	 */
	public function update() {
		list ( $Schema, $table ) = $this->_loadSchema();
		$this->_update($Schema, $table);
	}
	/**
	 * Prepares the Schema objects for database operations.
	 *
	 * @return void
	 */
	protected function _loadSchema() {
		$name = $plugin = null;
		if (!empty($this->params['name'])) {
			$name = $this->params['name'];
		}
		if (!empty($this->params['plugin'])) {
			$plugin = $this->params['plugin'];
		}

		if (!empty($this->params['dry'])) {
			$this->_dry = true;
			$this->out(__d('cake_console', 'Performing a dry run.'));
		}

		$options = array(
				'name' => $name,
				'plugin' => $plugin,
				'connection' => $this->params['connection']);
		if (!empty($this->params['snapshot'])) {
			$fileName = rtrim($this->Schema->file, '.php');
			$options['file'] = $fileName . '_' . $this->params['snapshot'] . '.php';
		}

		$Schema = $this->Schema->load($options);

		if (!$Schema) {
			$this->err(
					__d('cake_console', 'The chosen schema could not be loaded. Attempted to load:'));
			$this->err(
					__d('cake_console', 'File: %s', $this->Schema->path . DS . $this->Schema->file));
			$this->err(__d('cake_console', 'Name: %s', $this->Schema->name));
			return $this->_stop();
		}
		$table = null;
		if (isset($this->args[1])) {
			$table = $this->args[1];
		}

		$this->setupModels($Schema);

		return array(&$Schema, $table);
	}

	/**
	 * Override models in schema
	 *
	 * @param CakeSchema $Schema
	 */
	protected function setupModels(CakeSchema &$Schema) {
		$models = false;
		if (!empty($this->params['models'])) {
			$models = explode(',', $this->params['models']);
		}

		$db = ConnectionManager::getDataSource($this->Schema->connection);

		// Process any models for HABTM
		foreach ($models as $model) {

			list ( $plugin, $model ) = pluginSplit(trim($model));
			$model_tags = sprintf('%s_tags', strtolower(Inflector::pluralize($model)));
			$Schema->tables[$model_tags] = $Schema->tables['model_tags'];

			// Get Primary Key information
			$model_class = $model;
			if ($plugin) {
				$type = $plugin . '.Model';
			} else {
				$type = 'Model';
			}
			App::uses($model_class, $type);
			$Model = ClassRegistry::init($model_class);

			$primary_key = $Model->primaryKey;
			$metadata = $db->describe($Model);

			// Check for integer
			$id_field = $metadata[$primary_key];
			$id_field['key'] = 'index';
			$id_name = strtolower($model) . '_id';

			// Add id field
			$Schema->tables[$model_tags][$id_name] = $id_field;

			// Add Index for it
			$Schema->tables[$model_tags]['indexes'][$id_name] = array('column' => $id_name);
		}

		// Kill the model_tags table before returning.
		unset($Schema->tables['model_tags']);
	}

	/**
	 * Create database from Schema object
	 * Should be called via the run method
	 *
	 * @param CakeSchema $Schema
	 *        	The schema instance to create.
	 * @param string $table
	 *        	The table name.
	 * @return void
	 */
	protected function _create(CakeSchema $Schema, $table = null) {
		$db = ConnectionManager::getDataSource($this->Schema->connection);

		$drop = $create = array();

		if (!$table) {
			foreach ($Schema->tables as $table => $fields) {
				$drop[$table] = $db->dropSchema($Schema, $table);
				$create[$table] = $db->createSchema($Schema, $table);
			}
		} elseif (isset($Schema->tables[$table])) {
			$drop[$table] = $db->dropSchema($Schema, $table);
			$create[$table] = $db->createSchema($Schema, $table);
		}
		if (empty($drop) || empty($create)) {
			$this->out(__d('cake_console', 'Schema is up to date.'));
			return $this->_stop();
		}

		$this->out("\n" . __d('cake_console', 'The following table(s) will be dropped.'));
		$this->out(array_keys($drop));

		if (!empty($this->params['yes']) || $this->in(
				__d('cake_console', 'Are you sure you want to drop the table(s)?'), array('y', 'n'),
				'n') === 'y') {
			$this->out(__d('cake_console', 'Dropping table(s).'));
			$this->_run($drop, 'drop', $Schema);
		}

		$this->out("\n" . __d('cake_console', 'The following table(s) will be created.'));
		$this->out(array_keys($create));

		if (!empty($this->params['yes']) || $this->in(
				__d('cake_console', 'Are you sure you want to create the table(s)?'),
				array('y', 'n'), 'y') === 'y') {
			$this->out(__d('cake_console', 'Creating table(s).'));
			$this->_run($create, 'create', $Schema);
		}
		$this->out(__d('cake_console', 'End create.'));
	}

	/**
	 * Update database with Schema object
	 * Should be called via the run method
	 *
	 * @param
	 *        	CakeSchema &$Schema The schema instance
	 * @param string $table
	 *        	The table name.
	 * @return void
	 */
	protected function _update(&$Schema, $table = null) {
		$db = ConnectionManager::getDataSource($this->Schema->connection);

		$this->out(__d('cake_console', 'Comparing Database to Schema...'));
		$options = array();
		if (isset($this->params['force'])) {
			$options['models'] = false;
		}
		$Old = $this->Schema->read($options);
		$compare = $this->Schema->compare($Old, $Schema);

		$contents = array();

		if (empty($table)) {
			foreach ($compare as $table => $changes) {
				if (isset($compare[$table]['create'])) {
					$contents[$table] = $db->createSchema($Schema, $table);
				} else {
					$contents[$table] = $db->alterSchema(array($table => $compare[$table]), $table);
				}
			}
		} elseif (isset($compare[$table])) {
			if (isset($compare[$table]['create'])) {
				$contents[$table] = $db->createSchema($Schema, $table);
			} else {
				$contents[$table] = $db->alterSchema(array($table => $compare[$table]), $table);
			}
		}

		if (empty($contents)) {
			$this->out(__d('cake_console', 'Schema is up to date.'));
			return $this->_stop();
		}

		$this->out("\n" . __d('cake_console', 'The following statements will run.'));
		$this->out(array_map('trim', $contents));
		if (!empty($this->params['yes']) || $this->in(
				__d('cake_console', 'Are you sure you want to alter the tables?'), array('y', 'n'),
				'n') === 'y') {
			$this->out();
			$this->out(__d('cake_console', 'Updating Database...'));
			$this->_run($contents, 'update', $Schema);
		}

		$this->out(__d('cake_console', 'End update.'));
	}

	/**
	 * Runs sql from _create() or _update()
	 *
	 * @param array $contents
	 *        	The contents to execute.
	 * @param string $event
	 *        	The event to fire
	 * @param CakeSchema $Schema
	 *        	The schema instance.
	 * @return void
	 */
	protected function _run($contents, $event, CakeSchema $Schema) {
		if (empty($contents)) {
			$this->err(__d('cake_console', 'Sql could not be run'));
			return;
		}
		Configure::write('debug', 2);
		$db = ConnectionManager::getDataSource($this->Schema->connection);

		foreach ($contents as $table => $sql) {
			if (empty($sql)) {
				$this->out(__d('cake_console', '%s is up to date.', $table));
			} else {
				if ($this->_dry === true) {
					$this->out(__d('cake_console', 'Dry run for %s :', $table));
					$this->out($sql);
				} else {
					if (!$Schema->before(array($event => $table))) {
						return false;
					}
					$error = null;
					try {
						$db->execute($sql);
					} catch (PDOException $e) {
						$error = $table . ': ' . $e->getMessage();
					}

					$Schema->after(array($event => $table, 'errors' => $error));

					if (!empty($error)) {
						$this->err($error);
					} else {
						$this->out(__d('cake_console', '%s updated.', $table));
					}
				}
			}
		}
	}

	/**
	 * Gets the option parser instance and configures it.
	 *
	 * @return ConsoleOptionParser
	 */
	public function getOptionParser() {
		$parser = parent::getOptionParser();

		$connection = array(
				'short' => 'c',
				'help' => __d('cake_console', 'Set the db config to use.'),
				'default' => 'default');
		$models = array(
				'short' => 'm',
				'help' => __d('cake_console', 'Specify models for HABTM as comma separated list.'));
		$write = array('help' => __d('cake_console', 'Write the dumped SQL to a file.'));
		$yes = array(
				'short' => 'y',
				'help' => __d('cake_console', 'Do not prompt for confirmation. Be careful!'),
				'boolean' => true);
		$dry = array(
				'help' => __d('cake_console',
						'Perform a dry run on create and update commands. Queries will be output instead of run.'),
				'boolean' => true);
		$parser->description(
				__d('cake_console',
						'The Tag Schema Shell generates a schema object from the database and updates the database from the schema.'))->addSubcommand(
				'create',
				array(
						'help' => __d('cake_console',
								'Drop and create tables based on the schema file.'),
						'parser' => array(
								'options' => compact('connection', 'dry', 'yes', 'models'),
								'args' => array())))->addSubcommand('update',
				array(
						'help' => __d('cake_console', 'Alter the tables based on the schema file.'),
						'parser' => array(
								'options' => compact('connection', 'dry', 'yes', 'models'),
								'args' => array(
										'name' => array(
												'help' => __d('cake_console',
														'Name of schema to use.')),
										'table' => array(
												'help' => __d('cake_console',
														'Only create the specified table.'))))));

		return $parser;
	}
}