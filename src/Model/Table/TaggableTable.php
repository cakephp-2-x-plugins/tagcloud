<?php
namespace TagCloud\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

/**
 * Tag Model.
 *
 * Tags are used to add descriptive cross-references to other data.
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @version 1.0.0
 * @package Plugin\TagCloud\Model
 */
class TaggableTable extends Table
{

    /**
     *
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        // @todo fix this before uncommenting it.
        //$this->addBehavior('TagCloud.Taggable');

        // Information for the relationships.

        $throughTable = $this->throughTable();

        $this->belongsToMany($this->getTagTableAlias(), [
            'className' => 'TagCloud.Tags',
            'through' => $throughTable,
            'foreignKey' => $this->getJoinForeignKey(),
            'targetForeignKey' => 'tag_id'
        ]);
    }

    /**
     *
     * @return \Cake\ORM\Table
     */
    public function throughTable()
    {
        // Create the table from the registry
        $joinTableName = $this->getJoinTableName();
        $joinTableAlias = 'Join' . Inflector::camelize($joinTableName);

        if (TableRegistry::exists($joinTableAlias)) {
            $table = TableRegistry::get($joinTableAlias);
        } else {

            $table = TableRegistry::get($joinTableAlias, [
                'className' => 'TagCloud.TagsJoins'
            ]);

            $table->table($joinTableName);

            // Add some relationships
            $table->belongsTo($this->alias());
            $table->belongsTo($this->getTagTableAlias(), [
                'foreignKey' => 'tag_id',
                'className' => 'TagCloud.Tags'
            ]);
        }
        return $table;
    }

    /**
     *
     * @return string
     */
    protected function getJoinForeignKey()
    {
        return sprintf('%s_id', Inflector::underscore(Inflector::singularize($this->alias())));
    }

    /**
     *
     * @return string
     */
    protected function getJoinTableName()
    {
        return sprintf('%s_tags', $this->table());
    }

    /**
     *
     * @return string
     */
    protected function getTagTableAlias()
    {
        return sprintf('%sTags', $this->alias());
    }
}