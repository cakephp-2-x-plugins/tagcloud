<?php
namespace TagCloud\Model\Table;

use TagCloud\Model\Entity\Tag;
// use TagCloud\Model\Entity\TagCount;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
// use Cake\Validation\Validator;

/**
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @version 3.0
 */
class TagsTable extends Table
{

    /**
     *
     * @param array $config            
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        
        $this->table('tags');
        
        if ($alias = $this->getParentAlias()) {
            $this->belongsToMany($alias, [
                'through' => $this->throughTable()
            ]);
        }
    }

    /**
     *
     * @param unknown $name            
     * @return boolean
     */
    public function getId($name)
    {
        $tag = $this->findByTag($name)->first();
        
        if ($tag) {
            return $tag->id;
        }
        
        return false;
    }

    /**
     *
     * @return \Cake\ORM\Table
     */
    public function throughTable()
    {
        $joinTableName = $this->getJoinTableName();
        $joinTableAlias = 'Join' . Inflector::camelize($joinTableName);
        
        // Create the table from the registry
        if (TableRegistry::exists($joinTableAlias)) {
            $table = TableRegistry::get($joinTableAlias);
        } else {
            $table = TableRegistry::get($joinTableAlias, [
                'className' => 'TagCloud.TagsJoinsTable'
            ]);
            $table->table($joinTableName);
            
            // Add some relationships
            $table->belongsTo($this->getParentAlias());
            $table->belongsTo($this->alias(), [
                'foreignKey' => 'tag_id',
                'className' => 'TagCloud.Tags'
            ]);
        }
        
        return $table;
    }

    protected function getJoinTableName()
    {
        $parent = $this->getParentAlias();
        
        if (! empty($this->$parent)) {
            $tableName = $this->$parent->table();
        } else {
            $tableName = 'all_comics';
        }
        
        return sprintf('%s_tags', $tableName);
    }

    /**
     *
     * @return string boolean
     */
    protected function getParentAlias()
    {
        if (preg_match('/^(\w+)Tags$/', $this->alias(), $matches)) {
            // Return just the parent's alias.
            return $matches[1];
        }
        
        return false;
    }
}
