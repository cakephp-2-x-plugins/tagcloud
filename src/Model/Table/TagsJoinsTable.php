<?php
namespace TagCloud\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\ResultSet;
use Cake\ORM\Table;
use Cake\Utility\Inflector;

/**
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @version 3.0
 */
class TagsJoinsTable extends Table {
	/**
	 *
	 * @param array $config
	 */
	public function initialize(array $config) {
		$this->entityClass('TagCloud.TagCount');
	}

	/**
	 * Get a list of tags and, optionally, counts.
	 *
	 * @param string $stripId
	 *        	Strip ID
	 * @param boolean $justTags
	 *        	If TRUE, return only an array of tag names. (Default: TRUE)
	 * @return Query
	 * @since 3.0
	 * @access public
	 */
	public function findClouded(Query $query, array $options) {
		$tagAlias = $this->getTagAlias();
		$tagField = $this->getTagField($tagAlias);

		$query->select([$tagField, 'count' => $query->func()
			->count('*')])
			->group($tagField)
			->having('`count` > 0');

		$query->contain($this->getBelongsToTables())
			->formatResults([$this, 'formatAsList']);

		if (!empty($options['where'])) {
			$query->where($options['where']);
			unset($options['where']);
		}

		return $query;
	}

	/**
	 *
	 * @param ResultSet $results
	 * @param Query $query
	 * @return multitype:NULL
	 */
	public function formatAsList(ResultSet $results, Query $query) {
		$tagList = [];

		$tagAlias = Inflector::underscore(Inflector::singularize($this->getTagAlias()));

		foreach ($results as $result) {
			$tagList[$result->$tagAlias->tag] = $result->count;
		}

		return $tagList;
	}

	/**
	 *
	 * @return string boolean
	 */
	protected function getParentAlias() {
		if (preg_match('/^(\w+)Tags$/', $this->alias(), $matches)) {
			// Return just the parent's alias.
			return $matches[1];
		}

		return false;
	}

	/**
	 *
	 * @return string boolean
	 */
	protected function getTagAlias() {
		$associations = $this->associations()->type('BelongsTo');

		foreach ($associations as $belongsTo) {

			if (preg_match('/^\w+Tags$/', $belongsTo->alias())) {
				return $belongsTo->alias();
			}
		}

		return false;
	}

	/**
	 *
	 * @return string[]
	 */
	protected function getBelongsToTables() {
		$tables = [];

		$associations = $this->associations()->type('BelongsTo');

		foreach ($associations as $belongsTo) {
			$tables[] = $belongsTo->alias();
		}

		return $tables;
	}

	/**
	 *
	 * @return string
	 */
	protected function getTagField($alias = false) {
		if (!$alias) {
			$alias = $this->getTagAlias;
		}
		return sprintf('%s.tag', $alias);
	}
}