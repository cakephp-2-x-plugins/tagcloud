<?php
namespace TagCloud\Model\Behavior;

use TagCloud\Model\Entity\Tag;
use Cake\ORM\Behavior;
use Cake\ORM\Query;
use Cake\ORM\Table;

/**
 * Tag Behavior class file.
 *
 * Model Behavior to support tags.
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @version 3.0
 */
class TaggableBehavior extends Behavior
{

    /**
     * Note: separator can be several characters and will work with all of them.
     *
     * @var string[] Default configuration.
     */
    public $_defaultConfig = array(
        'table_label' => 'tags',
        'tag_label' => 'tag',
        'separator' => ','
    );

    /**
     * Initiate behavior for the model using specified settings.
     *
     * @param object $Model
     *            Model using the behavior
     * @param array $settings
     *            Settings to override for model.
     *
     * @access public
     */
    public function initialize(array $config)
    {
        if (empty($this->settings[$Model->alias])) {
            $this->settings[$Model->alias] = $this->defaultSettings;
        }

        if (is_array($settings)) {
            $this->settings[$Model->alias] = array_merge($this->settings[$Model->alias], $settings);
        }
    }

    /**
     * Callback to run before a model is saved.
     * Used to set up Tag for Model.
     *
     * @param object $Model
     *            Model about to be saved.
     * @param array $options
     *
     * @access public
     * @since 1.0
     */
    public function beforeSave(Model $Model, $options = array())
    {
        $tagAlias = sprintf('%sTag', $Model->alias);

        // Define the new tag model
        $TagModel = $Model->$tagAlias;
        if ($Model->hasField($this->settings[$Model->alias]['table_label']) && $TagModel->hasField($this->settings[$Model->alias]['tag_label'])) {

            // Parse out all of the tags into a list.
            $tagList = $this->parseTags($Model->data[$Model->alias][$this->settings[$Model->alias]['table_label']], $this->settings[$Model->alias]);

            // New tag array to store tag id and names from db
            $tagInfo = array();

            foreach ($tagList as $tag) {
                if ($result = $TagModel->find($this->settings[$Model->alias]['tag_label'] . " LIKE '" . $tag . "'")) {
                    $tagInfo[] = $result[$tagAlias]['id'];
                } else {
                    $TagModel->save(array(
                        'id' => '',
                        $this->settings[$Model->alias]['tag_label'] => $tag
                    ));
                    $tagInfo[] = sprintf($TagModel->getLastInsertID());
                }
                unset($result);
            }

            // This prepares the linking table data...
            $Model->data[$tagAlias][$tagAlias] = $tagInfo;
            // This formats the tags field before save...
            $Model->data[$Model->alias][$this->settings[$Model->alias]['table_label']] = implode(', ', $tagList);
        }

        return true;
    }

    /**
     * Parse the tag string and return a properly formatted array
     *
     * @param string $tagString
     *            String.
     * @param array $settings
     *            Settings to use (looks for 'separator' and 'length')
     * @return string Tag for given string.
     *
     * @access protected
     * @since 1.0
     */
    protected function parseTags($tagString, $settings)
    {
        $tagString = strtolower($tagString);
        $illegalCharRegEx = sprintf('/[^a-z0-9%s ]/i', $settings['separator']);
        $separatorRegex = sprintf('/[%s]/', $settings['separator']);
        $duplicateSeparatorRegEx = sprintf('/[%s]{2,}/', $settings['separator']);

        // remove any illegal characters
        $tagString = preg_replace($illegalCharRegEx, '', $tagString);
        // remove double (or triple, etc.) seperators. Using substr on separator to support multiple characters.
        $tagString = preg_replace($duplicateSeparatorRegEx, substr($settings['separator'], 0, 1), $tagString);

        // Split up the cleaned string
        $tags = preg_split('/' . $settings['separator'] . '/', $tagString);
        $result = array();

        foreach ($tags as $tag) {
            // Do a little more cleanup.
            $tag = strtolower(trim($tag));
            // Make sure we didn't just find spaces.
            if (strlen($tag) > 0) {
                $result[] = $tag;
            }
        }

        return $result;
    }
}