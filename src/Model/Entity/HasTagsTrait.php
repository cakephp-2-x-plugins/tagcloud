<?php
namespace TagCloud\Model\Entity;

use Cake\Routing\Router;
use Cake\Utility\Inflector;
use InputFilter\Utility\InputFilter;

/**
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @version 3.0
 */
trait HasTagsTrait
{

    /**
     *
     * @param string $full
     */
    public function tags($full = false)
    {
        $tagsAlias = $this->tagsAlias();

        if (empty($this->$tagsAlias)) {
            return $this->tagsFromList($full);
        }

        $output = [];

        foreach ($this->$tagsAlias as $tag) {
            $output[] = sprintf('<a href="%s">%s</a>', $this->tagUrl($tag->tag, $full), $tag->tag);
        }

        sort($output);

        return join(', ', $output);
    }

    /**
     * Backup for tags() to extract them from the list if necessary.
     *
     * @param string $full
     */
    public function tagsFromList($full = false)
    {
        // @todo See if we should pull from database instead. Probably not faster though.
        if (empty($this->tag_list)) {
            $tags = [
                __('none')
            ];
        } else {
            $tags = explode(',', $this->tag_list);
        }

        // @todo Support separators better.
        $output = [];
        foreach ($tags as $tag) {
            // Remove any left over whitespace.
            $tag = trim($tag);
            $output[] = sprintf('<a href="%s">%s</a>', $this->tagUrl($tag, $full), $tag);
        }

        sort($output);

        return join(', ', $output);
    }

    /**
     * Return a url for the tag.
     *
     * Needs to be overrided in the class. The default method does not work in real use.
     *
     * @param string $tag
     * @return string
     */
    public function tagUrl($tag, $full = false)
    {
        // sanitize tag (alphanumeric plus spaces)
        $tag = InputFilter::slugSpace($tag);

        return Router::url([
            'controller' => 'Tags',
            'action' => 'show',
            '?' => [
                'tag' => $tag
            ],
            '_full' => $full
        ]);
    }

    /**
     * @return string
     */
    protected function tagsAlias() {
        $classSpace = explode('/', __CLASS__);
        $class = array_pop($classSpace);

        return Inflector::pluralize($class) . 'Tags';
    }
}