<?php
namespace TagCloud\View\Helper;

use Cake\View\Helper;

/**
 * A Helper to allow for generation of tag clouds easily on a page.
 *
 * Originally based on TagcloudHelper by syl-via@go2.pl & Suhail Doshi.
 * The original version is available here: http://bakery.cakephp.org/articles/syl-via/2012/11/14/tagcloud_helper_cakephp_2
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @license BSD
 * @version 3.0
 */
class TagCloudHelper extends Helper
{

    /**
     * Helpers to include.
     *
     * @var string[]
     */
    public $helpers = [
        'Html'
    ];

    /**
     * Default configuration.
     *
     * @var string[string]
     */
    public $_defaultConfig = [
        'url_function' => false,
			/* font sizes */
			'min_size' => 10,
        'max_size' => 50,
			/* colors */
			'red' => 110,
        'green' => 105,
        'blue' => 255,
			/* div size */
			'div_size' => '100%',
			/* show frequencies */
			'show_frequencies' => false,
			/* Shuffle tags */
			'shuffle' => true
    ];

    /**
     * Show a tag cloud on the page.
     *
     * @author Jason Burgess
     * @author syl-via
     * @param array $tags
     *            Tags to build the cloud from. Typically uses the input from Tag::all().
     * @param array $options
     *            Settings to override for current cloud.
     * @return string Tag cloud to output
     * @since 1.1
     */
    public function show($tags, $options = array())
    {
        $options = array_merge($this->config(), $options);

        $data = self::formulateTagCloud($tags, $options);

        if ($options['shuffle']) {
            $data = self::shuffleTags($data);
        }

        /* Build cloud */
        $cloud = "<div style=\"width: {$options['div_size']}\">";
        foreach ($data as $word => $values) {
            $cloud .= "<span style=\"font-size: {$values['size']}px; color: {$values['color']};\">";
            $title = str_replace(" ", "&nbsp;", $word);

            if ($options['url_function']) {
                $cloud .= $this->Html->link($title, $options['url_function']($word), array(
                    'escapeTitle' => false
                ));
            } else {
                $cloud .= $title;
            }

            if ($options['show_frequencies']) {
                $cloud .= " <small>({$values['score']})</samll> ";
            }

            $cloud .= "</span> ";
        }
        $cloud .= "</div>";
        return $cloud;
    }

    /**
     * Main process of formulating the cloud.
     *
     * @author Jason Burgess
     * @author Suhail Doshi
     *
     * @param array $dataSet
     *            Cloud data.
     * @param array $options
     *            Settings to override for this instance.
     * @return array Processed tag cloud data.
     * @access protected
     * @since 1.1
     */
    protected static function formulateTagCloud($dataSet, $options = array())
    {
        // Return if we don't actually have any proper data to use.
        if (! is_array($dataSet)) {
            return false;
        }

        asort($dataSet); // Sort array accordingly.
                         // Retrieve extreme score values for normalization
        $options['min_score'] = intval(current($dataSet));
        $options['max_score'] = intval(end($dataSet));

        // Calculate other values here so they don't need to be repeatedly done.
        if ($options['min_score'] < 1) {
            $options['min_score'] = 1;
        }
        $spread = $options['max_score'] - $options['min_score'];
        if ($spread == 0) {
            $spread = 1;
        }

        // determine the font-size increment, this is the increase per tag quantity (times used)
        $options['step'] = ($options['max_size'] - $options['min_size']) / $spread;

        // Output array
        $output = array();

        // Populate new data array, with score value and size.
        foreach ($dataSet as $tagName => $score) {
            $size = self::getPercentSize($score, $options);
            $color = self::getColor($score, $options);
            $output[$tagName] = array(
                'score' => $score,
                'size' => $size,
                'color' => $color
            );
        }

        return $output;
    }

    /**
     * Get color for tag, based on score.
     *
     * @author Jason Burgess
     * @author syl-via
     * @param int $score
     *            Current score value for given item.
     * @param array $options
     *            Options for current cloud.
     * @return string Hex(ex: #000012) value of RGB.
     * @since 1.1
     * @access protected
     */
    protected static function getColor($score, $options = array())
    {
        $blue = floor($options['blue'] * ($score / $options['max_score']));
        return '#' . sprintf('%02s', dechex($options['red'])) . sprintf('%02s', dechex($options['green'])) . sprintf('%02s', dechex($blue));
    }

    /**
     * Get the size of a tag, based on the score.
     *
     * @author Jason Burgess
     * @author Suhail Doshi
     * @param int $score
     *            Current score value for given item.
     * @param array $options
     *            Options for current cloud.
     * @return int percentage for current tag.
     * @since 1.1
     * @access protected
     */
    protected static function getPercentSize($score, $options)
    {
        // Determine size based on current value and step-size.
        $size = $options['min_size'] + (($score - $options['min_score']) * $options['step']);
        return $size;
    }

    /**
     * Shuffle the tags so they aren't in any particular order.
     *
     * @author Jason Burgess
     * @author Suhail Doshi
     * @param array $tags
     *            An array of tags (takes an associative array)
     * @return array Shuffled array of tags for randomness.
     * @since 1.1
     * @access protected
     */
    protected static function shuffleTags($tags)
    {
        $new_arr = array();

        while (count($tags) > 0) {
            $val = array_rand($tags);
            $new_arr[$val] = $tags[$val];
            unset($tags[$val]);
        }

        return $new_arr;
    }
}